package Tests;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Contabilidad;
import clases.Factura;

public class TestsClientes {

	static Contabilidad contabilidad;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		contabilidad = new Contabilidad();
		contabilidad.getListaClientes().add(new Cliente("123456", "Alejandro", LocalDate.now()));
	}

	@Test
	public void testBuscarClienteInexistente() {
		String dni = "123512";
		Cliente actual = contabilidad.buscarCliente(dni);
		assertNull(actual);
	}

	@Test
	public void testBuscarClienteExistente() {
		Cliente clienteNuevo = new Cliente("1234R", "Fernando", LocalDate.now());
		contabilidad.getListaClientes().add(clienteNuevo);

		Cliente actual = contabilidad.buscarCliente("1234R");
		assertSame(clienteNuevo, actual);
	}

	@Test
	public void testBuscarClienteInexistenteHabiendoClientes() {
		String dni = "123512";
		Cliente actual = contabilidad.buscarCliente(dni);
		assertNull(actual);
	}

	@Test
	public void testBuscarClienteConVariosExistentes() {
		contabilidad.getListaClientes().clear();
		Cliente clienteAlex = new Cliente("34523456H", "Alex", LocalDate.now());
		contabilidad.getListaClientes().add(clienteAlex);
		Cliente clienteEsther = new Cliente("652566J", "Esther", LocalDate.now());
		contabilidad.getListaClientes().add(clienteEsther);

		Cliente actual = contabilidad.buscarCliente("652566J");
		assertSame(clienteEsther, actual);
	}

	@Test
	public void testAltaClienteListaVacia() {
		contabilidad.getListaClientes().clear();
		Cliente clienteEsther = new Cliente("652566J", "Esther", LocalDate.parse("2018-02-02"));
		contabilidad.altaCliente(clienteEsther);
		assertTrue(contabilidad.getListaClientes().contains(clienteEsther));

	}

	// Alta cliente con la lista llena
	@Test
	public void testAltaClienteLlena() {
		contabilidad.getListaClientes().clear();
		Cliente clienteEsther1 = new Cliente("652563J", "Esther1", LocalDate.parse("2008-02-02"));
		contabilidad.getListaClientes().add(clienteEsther1);

		Cliente clienteEsther = new Cliente("652566J", "Esther", LocalDate.parse("2018-02-02"));
		contabilidad.altaCliente(clienteEsther);
		assertTrue(contabilidad.getListaClientes().contains(clienteEsther));

	}

	// Con la lista vacia
	@Test
	public void testClienteMasAntiguoListaVacia() {
		contabilidad.getListaClientes().clear();
		Cliente expected = contabilidad.clienteMasAntiguo();
		assertNull(expected);
	}

	// Con un registro
	@Test
	public void testClienteMasAntiguo() {
		contabilidad.getListaClientes().clear();
		Cliente clienteEsther1 = new Cliente("652563J", "Esther1", LocalDate.parse("2008-02-02"));
		contabilidad.getListaClientes().add(clienteEsther1);

		Cliente expected = contabilidad.clienteMasAntiguo();
		assertSame(expected, clienteEsther1);
	}

	// Con un registro
	@Test
	public void testClienteMasAntiguoVarios() {
		contabilidad.getListaClientes().clear();
		Cliente clienteEsther1 = new Cliente("652563J", "Esther1", LocalDate.parse("2008-02-02"));
		contabilidad.getListaClientes().add(clienteEsther1);
		Cliente clienteEsther2 = new Cliente("6525asd", "Esther2", LocalDate.parse("2016-02-02"));
		contabilidad.getListaClientes().add(clienteEsther2);

		Cliente expected = contabilidad.clienteMasAntiguo();
		assertSame(expected, clienteEsther1);
	}

	// Con un registro
	@Test
	public void testClienteMasAntiguoVariosDelMismo() {
		contabilidad.getListaClientes().clear();
		Cliente clienteEsther1 = new Cliente("652563J", "Esther1", LocalDate.parse("2008-02-02"));
		contabilidad.getListaClientes().add(clienteEsther1);
		Cliente clienteEsther2 = new Cliente("6525asd", "Esther2", LocalDate.parse("2016-02-02"));
		contabilidad.getListaClientes().add(clienteEsther2);
		Cliente clienteEsther3 = new Cliente("6525aasd", "Esther3", LocalDate.parse("2008-02-02"));
		contabilidad.getListaClientes().add(clienteEsther3);

		Cliente expected = contabilidad.clienteMasAntiguo();
		assertSame(expected, clienteEsther1);
	}

	// eliminar un cliente
	@Test
	public void TestEliminarCliente() {
		contabilidad.getListaClientes().clear();
		contabilidad.getListaFacturas().clear();
		Cliente clienteEsther1 = new Cliente("652563J", "Esther1", LocalDate.parse("2008-02-02"));
		contabilidad.getListaClientes().add(clienteEsther1);
		contabilidad.eliminarCliente("652563J");
		assertFalse(contabilidad.getListaClientes().contains(clienteEsther1));

	}

	// eliminar varios clientes
	@Test
	public void TestEliminarMultiple() {
		contabilidad.getListaClientes().clear();
		contabilidad.getListaFacturas().clear();
		Cliente clienteEsther1 = new Cliente("652563J", "Esther1", LocalDate.parse("2008-02-02"));
		contabilidad.getListaClientes().add(clienteEsther1);
		Cliente clienteEsther3 = new Cliente("6525aasd", "Esther3", LocalDate.parse("2008-02-02"));
		contabilidad.getListaClientes().add(clienteEsther3);
		contabilidad.eliminarCliente("652563J");
		assertTrue(contabilidad.getListaClientes().contains(clienteEsther3));

	}
	
	// contar las facturas de un cliente
		@Test
		public void cantidadFacturasPorCliente() {
			contabilidad.getListaClientes().clear();
			contabilidad.getListaFacturas().clear();
			Cliente clienteEsther1 = new Cliente("652563J", "Esther1", LocalDate.parse("2008-02-02"));
			Factura factura = new Factura("asd", LocalDate.now(), "p1", 2, 1, clienteEsther1);
			Factura factura2 = new Factura("asdf", LocalDate.now(), "p2", 2, 1, clienteEsther1);
			contabilidad.getListaFacturas().add(factura);
			contabilidad.getListaFacturas().add(factura2);
			contabilidad.getListaClientes().add(clienteEsther1);
			int cantidad =contabilidad.cantidadFacturasPorCliente("652563J");
			assertEquals(cantidad, 2);

		}

	// eliminar clientes varias facturas
	@Test
	public void TestEliminarClienteMultifactura() {
		contabilidad.getListaClientes().clear();
		contabilidad.getListaFacturas().clear();
		Cliente clienteEsther1 = new Cliente("652563J", "Esther1", LocalDate.parse("2008-02-02"));
		Factura factura = new Factura("asd", LocalDate.now(), "p1", 2, 1, clienteEsther1);
		Factura factura2 = new Factura("asdf", LocalDate.now(), "p2", 2, 1, clienteEsther1);
		contabilidad.getListaFacturas().add(factura);
		contabilidad.getListaFacturas().add(factura2);
		contabilidad.getListaClientes().add(clienteEsther1);
		contabilidad.eliminarCliente("652563J");
		assertTrue(contabilidad.getListaClientes().contains(clienteEsther1));

	}
}