package clases;

import java.util.ArrayList;

public class Contabilidad {
	private ArrayList<Cliente> listaClientes;
	private ArrayList<Factura> listaFacturas;

	public Contabilidad() {
		listaClientes = new ArrayList<>();
		listaFacturas = new ArrayList<>();
	}

	public Cliente buscarCliente(String dni) {
		for (Cliente cliente : listaClientes) {
			if (cliente.getDni().equals(dni)) {
				return cliente;
			}
		}
		return null;
	}

	public Factura buscarFactura(String codigo) {
		for (Factura factura : listaFacturas) {
			if (factura.getCodigoFactura().equalsIgnoreCase(codigo)) {
				return factura;
			}
		}
		return null;
	}

	public void altaCliente(Cliente cliente) {
		boolean encontrado = false;
		for (Cliente c : listaClientes) {
			if (c.getDni().equalsIgnoreCase(cliente.getDni())) {
				encontrado = true;
			}
		}
		if (!encontrado) {
			listaClientes.add(cliente);
		}
	}

	public void crearFactura(Factura factura) {
		boolean encontrado = false;
		for (Factura f : listaFacturas) {
			if (f.getCodigoFactura().equalsIgnoreCase(factura.getCodigoFactura())) {
				encontrado = true;
			}
		}
		if (!encontrado) {
			listaFacturas.add(factura);
		}
	}

	public Cliente clienteMasAntiguo() {
		Cliente cliente = null;
		for (Cliente c : listaClientes) {
			if (cliente == null || c.getFechaAlta().isBefore(cliente.getFechaAlta())) {
				cliente = c;
			}

		}

		return cliente;
	}

	public Factura facturaMasCara() {
		Factura factura = null;
		for (Factura f : listaFacturas) {
			if (factura == null || f.calcularPrecioTotal() > factura.calcularPrecioTotal()) {
				factura = f;
			}

		}
		return factura;
	}

	public float calcularFacturacionAnual(int anno) {
		float suma = 0;
		for (Factura factura : listaFacturas) {
			if (factura.getFecha().getYear() == anno) {
				suma += factura.calcularPrecioTotal();
			}
		}
		return suma;
	}

	public void asignarClienteAFactura(String dni, String codigoFactura) {
		Factura factura = buscarFactura(codigoFactura);
		if (factura != null) {
			Cliente cliente = buscarCliente(dni);
			if (cliente != null) {
				factura.setCliente(cliente);
			}
		}
	}

	public int cantidadFacturasPorCliente(String dni) {
		int aux = 0;
		for (Factura f : listaFacturas) {
			if (f.getCliente() != null && f.getCliente().getDni().equalsIgnoreCase(dni)) {
				aux++;
			}
		}
		return aux;
	}

	public void eliminarFactura(String codigo) {
		listaFacturas.remove(buscarFactura(codigo));
	}

	public void eliminarCliente(String dni) {

		if (cantidadFacturasPorCliente(dni) == 0) {
			listaClientes.remove(buscarCliente(dni));
		}

	}

	
	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}

	
	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	
	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	
	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

}