package Tests;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Contabilidad;
import clases.Factura;

public class TestFacturas {

	static Factura factura;
	static Factura facturaNull;
	static Cliente cliente;
	static Contabilidad contabilidad;
	static ArrayList<Factura> facturas;

	@BeforeClass
	public static void crearFacturaPrevioTest() {
		contabilidad = new Contabilidad();
		facturas = contabilidad.getListaFacturas();
		cliente = new Cliente("1234", "Alejandro", LocalDate.parse("2016-02-02"));
		factura = new Factura("1234", LocalDate.parse("2018-02-02"), "Producto1", 2.5f, 3, cliente);
		facturaNull = new Factura("12345", LocalDate.parse("2017-02-02"), "ProductoNull", 6f, 1, null);
		contabilidad.getListaClientes().add(cliente);
	}

	@Test
	public void testCalcularPrecioTotal() {
		facturas.clear();
		facturas.add(factura);
		float actual = factura.calcularPrecioTotal();
		float esperado = 7.5F;

		assertEquals(esperado, actual, 0.001);
	}

	// Buscar una factura
	@Test
	public void buscarFactura() {
		facturas.clear();
		facturas.add(factura);
		Factura esperada = contabilidad.buscarFactura("1234");
		assertSame(factura, esperada);
	}

	// Buscar entre varias facturas
	@Test
	public void buscarVariasFactura() {
		facturas.clear();
		facturas.add(factura);
		facturas.add(facturaNull);
		Factura esperada = contabilidad.buscarFactura("1234");
		assertSame(factura, esperada);
	}

	// A�adir factura
	@Test
	public void crearFacturaEnVacio() {
		facturas.clear();
		contabilidad.crearFactura(factura);
		assertTrue(facturas.contains(factura));
	}

	// A�adir entre varias facturas
	@Test
	public void crearFactura() {
		facturas.clear();
		facturas.add(facturaNull);
		contabilidad.crearFactura(factura);
		assertTrue(facturas.contains(factura));
	}

	// Sin ningun objeto
	@Test
	public void facturaMasCara() {
		facturas.clear();

		Factura resultado = contabilidad.facturaMasCara();
		assertNull(resultado);
	}

	// Sin varios elementos
	@Test
	public void facturaMasCaraConVarios() {
		facturas.clear();
		facturas.add(facturaNull);
		facturas.add(factura);
		Factura resultado = contabilidad.facturaMasCara();
		assertSame(factura, resultado);
	}

	// Sin elementos
	@Test
	public void calcularFacturacionAnual() {
		facturas.clear();

		float resultado = contabilidad.calcularFacturacionAnual(2018);
		assertEquals(resultado, 0, 0.001);
	}

	// Varios a�os
	@Test
	public void calcularFacturacionAnualVariosAnos() {
		facturas.clear();
		Factura f4 = new Factura("12345a", LocalDate.parse("2017-02-02"), "ProductoNull", 1f, 1, null);
		Factura f3 = new Factura("12345f", LocalDate.parse("2016-02-02"), "ProductoNull", 6f, 1, null);
		Factura f2 = new Factura("12345h", LocalDate.parse("2017-02-02"), "ProductoNull", 1f, 1, null);
		facturas.add(f4);
		facturas.add(f3);
		facturas.add(f2);
		float resultado = contabilidad.calcularFacturacionAnual(2017);
		assertEquals(resultado, 2, 0.001);
	}

	// factura por cliente
	@Test
	public void asignarClienteAFactura() {
		facturas.clear();
		facturas.add(facturaNull);
		contabilidad.asignarClienteAFactura("1234", "12345");
		assertSame(facturaNull.getCliente(), cliente);
	}

	// factura por cliente equivocada
	@Test
	public void asignarClienteAFacturaEquivocada() {
		facturas.clear();
		Cliente old = facturaNull.getCliente();
		facturas.add(facturaNull);
		contabilidad.asignarClienteAFactura("1234", "123456");
		assertSame(facturaNull.getCliente(), old);
	}

	// Eliminar factura
	@Test
	public void eliminarFactura() {
		facturas.clear();
		facturas.add(facturaNull);
		contabilidad.eliminarFactura("12345");
		assertFalse(facturas.contains(facturaNull));

	}

	// Eliminar factura equivocada
	@Test
	public void eliminarFacturaEquivocada() {
		facturas.clear();
		facturas.add(facturaNull);
		contabilidad.eliminarFactura("");
		assertTrue(facturas.contains(facturaNull));

	}

}